var path = require("path");

module.exports = {
    mode: "development",
    entry: {
        content: "./content.js",
        background: "./background.js"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js"
    }
};

