import * as $ from "jquery";
import { ImageVisuals } from "./highlightLinkImage.js";


export class HighlightPopup {
    constructor(week, data){
        this.data = data;
        this.current_week = week;
        this.visuals = new ImageVisuals();
    } 

    getHighlightsImageLinkAbsolutePositionInDocument(link){
        var viewport_position = $(link)[0].getBoundingClientRect();

        // We want the popup to display right next to the link, so the top has
        // to be the sum of the distance from the link to the top of the viewport
        // plus the height of what was scrolled on the page already.
        var top_value = viewport_position.top + $(window).scrollTop();

        return { "top": top_value, "left": viewport_position.left };
    }

    createClickableHighlightPictureElement(data){
        var video_picture_container = $("<div></div>").addClass("highlight-picture");
        var video_picture = $("<img alt=\"Highlight picture\"></img>").attr({
            "src": data.mediumImageUrl,
            "alt": "Highlight Image"
        });
        
        // Make sure the key exists, if it doesn't just add the picture with no link attached to it
        if (Object.prototype.hasOwnProperty.call(data, "videoFileUrl")){
            this.setSelectedVideoHighlightLinkEvent(video_picture, data.videoFileUrl, 700000);
        }

        $(video_picture_container).append(video_picture);

        return video_picture_container;
    }

    createHighlightVideoDescriptionElement(data){
        var video_description = $("<div></div>").addClass("highlight-description");
        var description = Object.prototype.hasOwnProperty.call(data, "briefHeadline") ? data.briefHeadline : "";
        var description_text_as_link_to_video = $("<a></a>").text(description);

        // The popup will only appear if the url for the highlight exists
        if (Object.prototype.hasOwnProperty.call(data, "videoFileUrl")){
            this.setSelectedVideoHighlightLinkEvent(description_text_as_link_to_video, data.videoFileUrl, 700000);
        }
        
        $(video_description).append(description_text_as_link_to_video);

        return video_description;
    }

    createHighlightElement(data){
        var container;
        var highlight_elements_container;
        var highlight_bitrates;
        
        // No point in creating a link image if there's no url associated to it
        if (!Object.keys(data).includes("videoFileUrl") || data.videoFileUrl == null){
            return false;
        }
        
        container = $("<div></div>").addClass("video-container");
        highlight_elements_container = $("<div></div>").addClass("highlight-elements");
        
        $(highlight_elements_container).append(
            this.createClickableHighlightPictureElement(data),
            this.createHighlightVideoDescriptionElement(data)
        );
        
        highlight_bitrates = this.createHighlightsBitrateOptions(data.videoBitRates);

        // Only display the bitrate options if there are more than one option available, i.e. doesn't return false
        if (highlight_bitrates !== false){
            $(highlight_elements_container).append(highlight_bitrates);   
        }
        
        $(container).append(highlight_elements_container);

        return container;
    }

    createHighlightsTableElement(player_id, week){
        // Need this since 'this' will be binded to the for loop later on
        var self = this;

        var highlight_element;

        var main_container = $("<div></div>").addClass("highlight-table-container");
        var player_data = this.data[player_id] == null ? null : this.data[player_id][week];

        // Player has no highlights
        if (player_data == null){
            $(".left-scroll-arrow, .right-scroll-arrow").addClass("scroll-no-highlights");
            $(main_container).append("<h1 id=\"no-highlights-text\">No highlights available</h1>");
            return main_container;
        }
        
        $(".left-scroll-arrow, .right-scroll-arrow").removeClass("scroll-no-highlights");

        // Only three videos are visible in the popup, so make the scroll visible if there are more than three since that
        // would mean some videos are hidden
        if (player_data.length > 3){
            $(".left-scroll-arrow, .right-scroll-arrow").removeClass("scroll-hidden");
        } else {
            $(".left-scroll-arrow, .right-scroll-arrow").addClass("scroll-hidden");
        }

        player_data.forEach(function (data){
            highlight_element = self.createHighlightElement(data);

            if (!highlight_element){
                return;
            }

            $(main_container).append(highlight_element);
        });

        return main_container;
    } 

    updatePopupHeader(player_name, player_id){
        // Header must exist to update the values
        if ($("#highlight-player-name").length === 0 && $("#highlight-player-id").length === 0){
            return;
        }

        $("#highlight-player-name").text(player_name);
        $("#highlight-player-id").text(player_id);
    }

    setPopupPositionOnPage(highlight_link){
        // Both elements must exist to set their position
        if ($(".highlights-popup").length === 0 || $(".highlight-arrow-container").length === 0 || $(highlight_link).length === 0){
            return;
        }

        var popup_view_positions_to_set = this.getHighlightsImageLinkAbsolutePositionInDocument(highlight_link);

        $(".highlights-popup").css({
            "top": popup_view_positions_to_set.top - 30,
            "left": popup_view_positions_to_set.left + $(highlight_link).width() + $(".highlight-arrow-container").width()
        });

        $(".highlight-arrow-container").css({
            "top": popup_view_positions_to_set.top - 10,
            "left": popup_view_positions_to_set.left + $(highlight_link).width()
        });
    }

    displayHighlightsTable(table){
        // Both the popup and table elements must exist
        if ($(".highlights-popup").length === 0 || !table){
            return;
        }

        var popup_table_container = $(".highlights-popup .highlight-table-container");
        if (popup_table_container.length === 0){
            $(".highlights-popup > .left-scroll-arrow").after(table);
            return;
        }

        // Avoid non-stop appending of videos by replacing what currently exists
        $(popup_table_container).replaceWith(table); 
    }

    setHighlightLinkEvents(){
        // Need this since this will be binded to the event later on
        var self = this;

        $(".highlight-link-image-container").on({

            "mouseup": function (event){
                // Preventing the row from being selected if the image link is clicked
                event.stopPropagation(); 
            },

            "click": function (event){
                // Preventing the row from being selected if the image link is clicked
                event.stopPropagation();
                
                var player_name = $(event.target).closest(".Ov-h").find(".ysf-player-name a.name").text();
                var player_id = parseInt($(event.target).closest(".Ov-h").find(".ysf-player-name a.name").attr("href").split("/").pop());

                // Hide the highlight popup is the same image link was clicked again
                if (parseInt($("#highlight-player-id").text()) === player_id){
                    $(".highlights-popup-container").toggle();
                    
                    if ($(".highlights-popup-container").css("display") === "none"){
                        $(this).removeClass("active-popup");
                    } else {
                        $(this).addClass("active-popup");
                    }

                    return;
                }
                
                // Every time a new popup opens, make the default selected week to the current one and hide the scroll arrows
                $(".week-select-dropdown > select").val(self.current_week);
                $(".left-scroll-arrow, .right-scroll-arrow").addClass("scroll-hidden");

                self.displayHighlightsTable(self.createHighlightsTableElement(player_id, self.current_week));
                self.updatePopupHeader(player_name, player_id);
                self.setPopupPositionOnPage($(event.target).closest(".highlight-link-image-container"));
                
                // Make sure that no other image link has the active-popup class
                $(".highlight-link-image-container").removeClass("active-popup");
                $(this).addClass("active-popup");
                
                // Toggle the popup after all the elements in it were updated
                if ($(".highlights-popup-container").css("display") === "none"){
                    $(".highlights-popup-container").toggle();
                }
            }
        });
    }
    
    setDocumentEvents(){
        $(document).click(function (event){
            var valid_classes = [".highlight-link-image-container", ".highlights-popup"];
            var clicked_element_contains_a_valid_class = valid_classes.some((_class) => $(event.target).closest(_class).length === 1);

            // Hide the popup if we click anywhere outside of the link or the popup itself
            if(!clicked_element_contains_a_valid_class){
                $(".highlights-popup-container").css("display", "none");
                $(".highlight-link-image-container").removeClass("active-popup");
            }
        });
    }
    
    setWeekSelectionDropdownEvents(){
        // Need this since this will be binded to the event later on
        var self = this;

        $(".week-select-dropdown > select").change(function (){
            var player_id = $("#highlight-player-id").text();

            $(".week-select-dropdown > select").val($(this).val());
           
            var highlights_table = self.createHighlightsTableElement(player_id, $(this).val());
            if ($(".highlights-popup .highlight-table-container").length === 0){
                $(".highlights-popup").append(highlights_table);
                return;
            }

            // Avoid non-stop appending of videos by replacing what already exists
            $(".highlights-popup .highlight-table-container").replaceWith(highlights_table); 
        }); 
    }

    createWeekSelectionButton(){
        var container = $("<div></div>").addClass("week-select-dropdown");
        
        // The dropdown selections will be all the weeks passed plus the current one
        var select_element = $("<select></select>");
        var week = 1;
        while (week <= this.current_week){
            $(select_element).append("<option value=\"" + week + "\">Week " + week + "</option>");
            week += 1;
        }
        
        $(select_element).last().attr("selected", "selected");
        $(select_element).val(this.current_week);
        $(container).append(select_element);

        return container;
    }

    createHighlightsPopupBox(){
        // Container, not visible to the user, that allows us to position the popup based on the page body
        var container = $("<div></div>").addClass("highlights-popup-container");
        $(container).css("display", "none");
        
        // Popup header elements
        var header = $("<div></div>").addClass("highlights-header");
        var title = $("<h1><span id=\"highlight-player-name\"></span><span id=\"highlight-player-id\"></span> highlights</h1>").attr("id", "highlights-title");
        var week_selection_dropdown = this.createWeekSelectionButton();
        $(header).append(title, week_selection_dropdown);

        // Scroll arrows
        var scroll_arrows = this.visuals.createScrollArrowImages();
        var left_arrow_container = $("<div class=\"left-scroll-arrow scroll-hidden\"></div>").append(scroll_arrows.left);
        var right_arrow_container = $("<div class=\"right-scroll-arrow scroll-hidden\"></div>").append(scroll_arrows.right);

        // Main popup window visible to the user
        var popup_window = $("<div></div>").addClass("highlights-popup");
        $(popup_window).append(header, left_arrow_container, right_arrow_container);

        // Arrow connecting the image link to the popup
        var arrow = this.visuals.createArrowImage();

        $(container).append(popup_window, arrow);

        return container;
    }

    displayHighlightsPopupBox(){
        var popup = this.createHighlightsPopupBox();
        $("body").prepend(popup);
    }

    createHighlightsBitrateOptions(bitrate_data){
        var self = this;

        var link_element;
        var bitrate_option_element;
        var resolution_choice;
        var bitrates_to_offer_as_options = [700000, 2000000, 5000000];
        var container = $("<div></div>").addClass("highlight-bitrate-options");
       
        // We need at least two bitrate options since the default one associated to the picture link already counts as one
        if (!bitrate_data || bitrate_data.length < 2){
            return false;
        }

        bitrate_data.forEach(function (data){
            if (!bitrates_to_offer_as_options.includes(data.bitrate)){
                return;
            }

            // No point in creating the bitrate option if the video url doesn't exist
            if (!Object.prototype.hasOwnProperty.call(data, "videoPath")){
                return;
            }

            // By removing the bitrate that exists from the array, this allows there to only be one link for each
            // bitrate we are offering
            bitrates_to_offer_as_options.splice(bitrates_to_offer_as_options.indexOf(data.bitrate), 1);

            switch(data.bitrate){
                case 700000:
                    resolution_choice = "Low";
                    break;
                case 2000000:
                    resolution_choice = "Mid";
                    break;
                case 5000000:
                    resolution_choice = "High";
                    break;
                default:
                    return;
            }

            bitrate_option_element = $("<div></div>").addClass("bitrate-option");
            link_element = $("<a>" + resolution_choice + "</a>");

            self.setSelectedVideoHighlightLinkEvent(link_element, data.videoPath, data.bitrate);

            $(bitrate_option_element).append(link_element);
            $(container).append(bitrate_option_element);
        });
        
        // Since we popped the bitrates from the array in the loop, we know that if there are at least 2 bitrates still in the array,
        // then there is only one avalable bitrate and we want at least two
        if (bitrates_to_offer_as_options.length >= 2){
            return false;
        }

        return container;
    }

    displayHighlightLinks(){
        var self = this;
        
        // Player status link must exist
        if ($("#team-roster .tablewrap:lt(2) tbody tr .player-status").length === 0){
            return;
        }

        $("#team-roster .tablewrap:lt(2) tbody tr .player").each(function (){
            // Skip missing players
            if ($(this).find("div.emptyplayer").length !== 0){
                return;
            }

            // The image link for the highlight will be on the left of to the player status card image.
            // Since the player status card has the "float: right" property, we have to set our image link
            // after the status card instead of before that way it will float to its left.
            $(this).find(".player-status").after(self.visuals.createSvgLinkImage());
        });
    }

    setScrollArrowsEvents(){
        const set_stroke = (width, elem) => { $(elem).find("polygon").css("stroke-width", width) }; 
        const set_scroll = (scroll) => { $(".highlight-table-container").animate({ scrollLeft: scroll }, "slow") }; 

        $(".left-scroll-arrow .scroll-arrow").on({
            mouseenter: function (){
                set_stroke(5, this);
            },
            mouseleave: function (){
                set_stroke(3, this);
            },
            click: function (event){
                event.stopPropagation();
                set_scroll("-=232px");
            }
        });

        $(".right-scroll-arrow .scroll-arrow").on({
            mouseenter: function (){
                set_stroke(5, this);
            },
            mouseleave: function (){
                set_stroke(3, this);
            },
            click: function (event){
                event.stopPropagation();
                set_scroll("+=232px");
            }
        });
    }

    getPositionsToSetForSingleVideoPopup(bitrate){
        var positions = {};

        // Extra interface elements like toolbars and scrollbars
        var extra_vewport_height_dimension = window.outerHeight - window.innerHeight;
        var extra_vewport_width_dimension = window.outerWidth - window.innerWidth;

        const set_position_values = (height, width) => { 
            positions.top = ((window.innerHeight - height) / 2) + extra_vewport_height_dimension;
            positions.left = ((window.innerWidth - width) / 2) + extra_vewport_width_dimension;
        };

        switch(bitrate){
            case 700000:
                set_position_values(400, 660);
                break;
            case 2000000:
                set_position_values(472, 788);
                break;
            case 5000000:
                set_position_values(728, 1244);
                break;
            default:
                positions.top = 0;
                positions.left = 0;
        }

        return positions;
    }

    getDimentionsToSetForSingleVideoPopup(bitrate){
        var dimensions = {};

        const set_dimension_values = (height, width) => { 
            dimensions.height = height;
            dimensions.width = width;
        };

        switch(bitrate){
            case 700000:
                set_dimension_values(380, 660);
                break;
            case 2000000:
                set_dimension_values(452, 788);
                break;
            case 5000000:
                set_dimension_values(708, 1244);
                break;
            default:
                set_dimension_values(0, 0);
        }

        return dimensions;
    }


    setSelectedVideoHighlightLinkEvent(element, url, bitrate){
        var self = this;
        var window_options = "toolbar=no, titlebar=no, location=yes, menubar=no, scrollbars=no, resizable=yes";

        $(element).click(function (event){
            event.preventDefault();

            var popup_positions = self.getPositionsToSetForSingleVideoPopup(bitrate);
            var popup_dimensions = self.getDimentionsToSetForSingleVideoPopup(bitrate);

            window_options = window_options.concat(",width=" + popup_dimensions.width + ",height=" + popup_dimensions.height + ",left=" + popup_positions.left + ",top=" + popup_positions.top); 
            
            this.popup_video = window.open(url, "_blank", window_options);
        });
    }

    init(){
        this.displayHighlightLinks(); 
        this.displayHighlightsPopupBox();
        this.setWeekSelectionDropdownEvents();
        this.setHighlightLinkEvents();
        this.setScrollArrowsEvents();
        this.setDocumentEvents();
    }
}
