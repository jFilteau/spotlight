import * as $ from "jquery";
import { HighlightPopup } from "./scripts/highlightsPopup.js";
import { Validators } from "./scripts/validators.js";
import { createFilteredDataToStore, updateHighlightsStorageData } from "./scripts/storageHandlers.js";
import { PlayerNotePopupModifier } from "./scripts/playerNotePopupModifier.js";


export function getPlayersAndTeamOnFantasyTeam(){
    var player_name;
    var player_id;
    var position;
    var team_abbr;
    var player_name_link_container;
    var team_and_position;

    var validator = new Validators();
    var name_to_id_and_team_abbr = {};

    if ($(".ysf-player-name").length === 0){
        throw new Error("Player tags not found.");
    }

    $(".ysf-player-name").each(function (){
        // Skip missing players
        if ($(this).find("div.emptyplayer").length !== 0){
            return;
        }
        
        player_name_link_container = $(this).find("a.name");
        if ($(player_name_link_container).length === 0){
            throw new Error("Player tags don't contain the player name tag.");
        }
            
        team_and_position = $(this).find("span.Fz-xxs");
        if ($(team_and_position).length === 0){
            throw new Error("Player tags don't contain the team and position tag.");
        }
        if ($(team_and_position).text().split("-").length < 2){
            throw new Error("Player position and team string does not contain two elements separated by a dash.");
        }

        position = $(team_and_position).text().split("-")[1].trim().toUpperCase();
        team_abbr = $(team_and_position).text().split("-")[0].trim().toUpperCase();
        if (!validator.validatePlayerPosition(position) || !validator.validateTeamAbbreviation(team_abbr)){
            return;
        }
        
        // We don't want defenses and the player name link must have at least one element after the /
        if ($(player_name_link_container).attr("href").split("/").length === 0 || position === "DEF"){
            return;
        }

        // The link must contain the player's yahoo id which is an integer
        player_id = parseInt($(player_name_link_container).attr("href").split("/").pop()); 
        if (isNaN(player_id)){
            return;
        }
        
        player_name = $(player_name_link_container).text();
        name_to_id_and_team_abbr[player_name] = { "team_abbr": team_abbr, "id": player_id };
    });

    return name_to_id_and_team_abbr;
}

export function getCurrentWeekNumber(){
    var words;
    var week_value;
    var week = $(".flyout-title"); 

    if($(week).length === 0){
        throw new Error("Current week tag not found.");
    }
    
    words = $(week).text().trim().split(" ");
    if (words.length < 2){
        throw new Error("Text inside the week tag is invalid.");
    }
    
    week_value = parseInt(words[1]);
    if (isNaN(week_value)){
        throw new Error("Week value inside the week tag is not a number.");
    }
 
    return week_value;
}

function run(){
    var current_week_number;
    var player_names_to_id_and_team_abbr;
    var stored_highlights;
    var message_to_send;
    var popup;
    var filtered_data;
    var player_note_modifier;

    try {
        // The extension can only start if both these values are available
        current_week_number = getCurrentWeekNumber();
        player_names_to_id_and_team_abbr = getPlayersAndTeamOnFantasyTeam();
    } catch (err) {
        console.debug(err);
        return;
    }

    stored_highlights = localStorage.getItem("highlights") === null ? null : JSON.parse(localStorage.getItem("highlights"));
    message_to_send = {
        "players": player_names_to_id_and_team_abbr,
        "week": current_week_number,
        "stored_highlights": stored_highlights
    };

    chrome.runtime.sendMessage(message_to_send, function (response){
        filtered_data = createFilteredDataToStore(response.data);
        updateHighlightsStorageData(filtered_data);

        popup = new HighlightPopup(current_week_number, filtered_data);
        popup.init();

        player_note_modifier = new PlayerNotePopupModifier(filtered_data.game_recaps);
        player_note_modifier.init();
    });
}

run();
