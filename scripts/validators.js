import { team_abbr_to_team_id } from "../mappings/teamAbbreviations.js";


export class Validators {
    constructor(){
        this.valid_positions = ["QB", "WR", "RB", "TE", "K"];
        this.valid_team_abbreviations = Object.keys(team_abbr_to_team_id);
    }

    validateTeamAbbreviation(abbr){
        return this.valid_team_abbreviations.includes(abbr);
    }

    validatePlayerPosition(position){
        return this.valid_positions.includes(position);
    }
}
