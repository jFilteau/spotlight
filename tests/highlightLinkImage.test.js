import * as $ from "jquery";
import { ImageVisuals } from "../scripts/highlightLinkImage.js";


describe("ImageVisuals", function (){
    describe("SVG element creation tests", function (){
        beforeEach(function (){
            this.visuals = new ImageVisuals();
        });

        it("Should call the node creation function three times", function (){
            spyOn(document, "createElementNS"); 
            this.visuals.createSvgLinkImage();
            
            expect(document.createElementNS).toHaveBeenCalledWith("http://www.w3.org/2000/svg", "svg");
            expect(document.createElementNS).toHaveBeenCalledWith("http://www.w3.org/2000/svg", "rect");
            expect(document.createElementNS).toHaveBeenCalledWith("http://www.w3.org/2000/svg", "polygon");
        });

        it("Should create the video svg html elements", function (){
            var link_image = this.visuals.createSvgLinkImage();
            
            expect(link_image.length).toBe(1);
            expect(link_image.hasClass("highlight-link-image-container")).toBe(true);
            expect(link_image.find(".highlight-link-image").length).toBe(2);
        });

        it("Should call the node creation function two times", function (){
            spyOn(document, "createElementNS"); 
            this.visuals.createArrowImage();
            
            expect(document.createElementNS).toHaveBeenCalledWith("http://www.w3.org/2000/svg", "svg");
            expect(document.createElementNS).toHaveBeenCalledWith("http://www.w3.org/2000/svg", "polygon");
        });

        it("Should create the arrow svg html elements", function (){
            var link_image = this.visuals.createArrowImage();
            
            expect(link_image.length).toBe(1);
            expect(link_image.hasClass("highlight-arrow-container")).toBe(true);
            expect(link_image.find(".highlight-arrow").length).toBe(1);
        });

        it("Should create the scroll arrows html elements", function (){
            var scroll_arrows = this.visuals.createScrollArrowImages(); 

            expect($(scroll_arrows.left).hasClass("scroll-arrow")).toEqual(true);
            expect($(scroll_arrows.right).hasClass("scroll-arrow")).toEqual(true);
            expect($(scroll_arrows.left).find(" > polygon").length).toBe(1);
            expect($(scroll_arrows.right).find(" > polygon").length).toBe(1);
        });
    });
});
