import * as $ from "jquery";


export class ImageVisuals{
    constructor(){
        this.xmlns = "http://www.w3.org/2000/svg";
    }

    createSvgLinkImage(){
        var container = $(document.createElementNS(this.xmlns, "svg")).attr({
            height: 16,
            width: 26,
            class: "highlight-link-image-container"
        });

        var main_rectangle = $(document.createElementNS(this.xmlns, "rect")).attr({
            width: 14,
            height: 12,
            x: 3,
            y: 1,
            rx: 3,
            ry: 3,
            points: "translate(0, 21)",
            class: "highlight-link-image"
        });

        var polygon = $(document.createElementNS(this.xmlns, "polygon")).attr({
            points: "17,5 17,9 24,12 24,2",
            "stroke-linejoin": "round",
            class: "highlight-link-image"
        });
        
        $(container).append(main_rectangle, polygon);

        return container;
    }

    createArrowImage(){
        var container = $(document.createElementNS(this.xmlns, "svg")).attr({
            height: 36,
            width: 15,
            class: "highlight-arrow-container"
        });
        
        var arrow = $(document.createElementNS(this.xmlns, "polygon")).attr({
            points: "0,17.5 15,0 15,36",
            class: "highlight-arrow"
        });

        $(container).append(arrow);

        return container;
    }

    createScrollArrowContainer(){
         var container = $(document.createElementNS(this.xmlns, "svg")).attr({
            height: 30,
            width: 30,
            class: "scroll-arrow"
        });
        
        return container;
    }

    createScrollArrowImages(){
        var right_scroll_container = this.createScrollArrowContainer();
        var right_scroll_arrow = $(document.createElementNS(this.xmlns, "polygon")).attr({
            points: "4,4 12,15 4,26 12,15",
            stroke: "black",
            "stroke-width": 3,
            "stroke-linejoin": "round"
        });

        var left_scroll_container = this.createScrollArrowContainer();
        var left_scroll_arrow = $(document.createElementNS(this.xmlns, "polygon")).attr({
            points: "26,4 18,15 26,26 18,15",
            stroke: "black",
            "stroke-width": 3,
            "stroke-linejoin": "round"
        });

        return { left: $(left_scroll_container).append(left_scroll_arrow), right: $(right_scroll_container).append(right_scroll_arrow) };
    }
}
