import * as $ from "jquery";
import { HighlightPopup } from "../scripts/highlightsPopup.js";


describe("HighlightPopup", function (){
    describe("DOM manipulation tests", function (){
        // Since we're only testing doing DOM manipulation, the data argument can be empty
        var popup = new HighlightPopup(1, {});
        
        beforeEach(function (){
            $("body").append("<div id=\"test-container\"></div>");
        });

        afterEach(function (){
            $("#test-container").remove();
        });

        // getHighlightsImageLinkAbsolutePositionInDocument
        it("Should return the position in the viewport", function (){
            $("#test-container").append("<div id=\"test\"></div>");
            $("#test").css("position", "fixed");

            var positions_to_test = [
                { positions: { top: 0, left: 0 }, expected_output: { top: 0, left: 0 } },
                { positions: { top: 10, left: 0 }, expected_output: { top: 10, left: 0 } },
                { positions: { top: 0, left: 10 }, expected_output: { top: 0, left: 10 } },
                { positions: { top: 10, left: 10 }, expected_output: { top: 10, left: 10 } },
                { positions: { top: -10, left: -10 }, expected_output: { top: -10, left: -10 } }
            ]; 
            positions_to_test.forEach(function (test){
                $("#test").css({ top: test.positions.top, left: test.positions.left });
                expect(popup.getHighlightsImageLinkAbsolutePositionInDocument($("#test"))).toEqual(test.expected_output);
            });
        }); 

        // updatePopupHeader 
        it("Should update the popup header text", function (){
            // Nothing updated since the tags don't exist
            popup.updatePopupHeader("Test name", 1);
            expect($("#highlight-player-name").length).toBe(0);
            expect($("#highlight-player-id").length).toBe(0);
            
            // Tags exist now so they should be updated
            $("#test-container").append("<div><span id=\"highlight-player-name\"></span><span id=\"highlight-player-id\"></span></div>");
            expect($("#highlight-player-name").length).toBe(1);
            expect($("#highlight-player-id").length).toBe(1);

            var names_and_ids_to_test = [
                { name: "", id: "" }, { name: "Test name", id: "" }, { name: "Test name", id: 1 }, { name: "Test name", id: "1" },
                { name: 123, id: "Test" }, { name: null, id: null }, { name: null, id: 1 }, { name: "Test name", id: null }
            ];
            names_and_ids_to_test.forEach(function (test){
                var expected_name = test.name === null ? "" : String(test.name); 
                var expected_id = test.id === null ? "" : String(test.id); 
                
                popup.updatePopupHeader(test.name, test.id);
                
                expect($("#highlight-player-name").text()).toBe(expected_name);
                expect($("#highlight-player-id").text()).toBe(expected_id);
            });
        });

        // setPopupPositionOnPage
        it("Should set the position of the popup on the page", function (){
            $("#test-container").css({ position: "absolute", top: 0, left: 0 }).append("<div id=\"test-highlight-link\"></div>");

            spyOn(popup, "getHighlightsImageLinkAbsolutePositionInDocument");

            // Nothing updated since the popup tags dont exist
            expect(popup.setPopupPositionOnPage($("#test-highlight-link"))).toEqual(undefined);
            expect(popup.getHighlightsImageLinkAbsolutePositionInDocument).not.toHaveBeenCalled();

            // Tags exist now so they should be updated
            $("#test-container").append("<div class=\"highlights-popup\"></div><div class=\"highlight-arrow-container\"></div>");
            $(".highlights-popup").css("position", "absolute");
            $(".highlight-arrow-container").css("position", "absolute");
            expect($(".highlights-popup").length).toBe(1);
            expect($(".highlight-arrow-container").length).toBe(1);
        
            var test_values = [
                { returned_value_of_spied_function: { top: 0, left: 0 }, popup_width: 1, arrow_width: 1, link_width: 1 },
                { returned_value_of_spied_function: { top: 10, left: 0 }, popup_width: 1, arrow_width: 1, link_width: 1 },
                { returned_value_of_spied_function: { top: 0, left: 10 }, popup_width: 1, arrow_width: 1, link_width: 1 },
                { returned_value_of_spied_function: { top: 10, left: 10 }, popup_width: 1, arrow_width: 1, link_width: 1 },
                { returned_value_of_spied_function: { top: -10, left: -10 }, popup_width: 1, arrow_width: 1, link_width: 1 }
            ]; 
            test_values.forEach(function (value){
                var popup_expected_top_position = value.returned_value_of_spied_function.top - 30;
                var popup_expected_left_position = value.returned_value_of_spied_function.left + value.link_width + value.arrow_width;
                var arrow_expected_top_position = value.returned_value_of_spied_function.top - 10;
                var arrow_expected_left_position = value.returned_value_of_spied_function.left + value.link_width;

                popup.getHighlightsImageLinkAbsolutePositionInDocument.and.returnValue(value.returned_value_of_spied_function);

                $("#test-highlight-link").width(value.link_width);
                $(".highlights-popup").width(value.popup_width);
                $(".highlight-arrow-container").width(value.arrow_width);
               
                popup.setPopupPositionOnPage($("#test-highlight-link"));

                expect(popup.getHighlightsImageLinkAbsolutePositionInDocument).toHaveBeenCalled();
                expect($(".highlights-popup").offset().top).toBe(popup_expected_top_position); 
                expect($(".highlights-popup").offset().left).toBe(popup_expected_left_position); 
                expect($(".highlight-arrow-container").offset().top).toBe(arrow_expected_top_position); 
                expect($(".highlight-arrow-container").offset().left).toBe(arrow_expected_left_position); 
            }); 
        });

        // displayHighlightsTable
        it("Should display the table element", function (){
            var table_element = "<div class=\"highlight-table-container\">Test table</div>";
            var replacement_table_element = "<div class=\"highlight-table-container\">Test replacement table</div>";

            // Nothing updated since the table element is null
            expect(popup.displayHighlightsTable(null)).toEqual(undefined);

            // Nothing updated since the popup tags dont exist
            expect(popup.displayHighlightsTable(table_element)).toEqual(undefined);

            // Tags exist now so they should be updated
            $("#test-container").append("<div class=\"highlights-popup\"><div class=\"left-scroll-arrow\"></div></div>");
            expect($(".highlights-popup").length).toBe(1);

            popup.displayHighlightsTable(table_element);
            expect($(".highlights-popup > .highlight-table-container").html()).toBe($(table_element).text());

            // Running it a second time should replace the existing table
            popup.displayHighlightsTable(replacement_table_element);
            expect($(".highlights-popup > .highlight-table-container").html()).toBe($(replacement_table_element).text());
        });
        
        // displayHighlightsPopupBox
        it("Should append the popup box container at the beginning of the body", function (){
            var test_popup = "<div>Test popup</div>";
            
            spyOn(popup, "createHighlightsPopupBox").and.returnValue(test_popup);
            popup.displayHighlightsPopupBox();

            expect(popup.createHighlightsPopupBox).toHaveBeenCalled();
            expect($("body > div").first()[0]).toEqual($(test_popup)[0]);
        });

        // displayHighlightLinks
        it("Should display the image link after the player status link", function (){
            var sample_visual = "<div>Sample image</div>";

            spyOn(popup.visuals, "createSvgLinkImage").and.returnValue(sample_visual);

            // Player status link doesn't exist
            expect(popup.displayHighlightLinks()).toEqual(undefined);
            expect(popup.visuals.createSvgLinkImage).not.toHaveBeenCalled();

            // Player status link exists
            $("#test-container").append("<div id=\"team-roster\"></div>");
            $("#team-roster").append("<div class=\"tablewrap\"></div>");
            $(".tablewrap").append("<table></table>");
            $(".tablewrap table").append("<tbody><tr><td><div class=\"player\"><div class=\"player-status\"></div></div></td></tr></tbody>");
            expect(popup.displayHighlightLinks()).toEqual(undefined);
            expect(popup.visuals.createSvgLinkImage).toHaveBeenCalled();
        });
    })

    describe("HTML element creation tests", function (){
        beforeEach(function (){
            this.popup = new HighlightPopup(1, {});
            $("body").append("<div id=\"test-container\"></div>");
        });
    
        afterEach(function (){
            $("#test-container").remove();
        });

        // createClickableHighlightPictureElement
        it("Should create a clickable image html element", function (){
            var self = this;

            spyOn(this.popup, "setSelectedVideoHighlightLinkEvent");

            var image_element;
            
            var test_values = [
                // All keys needed exist
                { 
                    values: { mediumImageUrl: "/test_image", videoFileUrl: "/test_video" },
                    expected: { has_class: true, img_length: 1, img_src: "/test_image" } 
                },
                
                // Missing the image url key
                { 
                    values: { videoFileUrl: "/test_video" },
                    expected: { has_class: true, img_length: 1, img_src: undefined } 
                },
                
                // Missing the video url key
                { 
                    values: { mediumImageUrl: "/test_image" },
                    expected: { has_class: true, img_length: 1, img_src: "/test_image" } 
                },
                
                // Missing both the video url and image url keys
                { 
                    values: {},
                    expected: { has_class: true, img_length: 1, img_src: undefined } 
                }
            ]
            
            test_values.forEach(function (test){
                image_element = self.popup.createClickableHighlightPictureElement(test.values);
                expect($(image_element).hasClass("highlight-picture")).toEqual(test.expected.has_class);
                expect($(image_element).find("img").length).toBe(test.expected.img_length);
                expect($(image_element).find("img").attr("src")).toBe(test.expected.img_src);
                expect(self.popup.setSelectedVideoHighlightLinkEvent).toHaveBeenCalled();
            });
        });

        // createHighlightVideoDescriptionElement
        it("Should create the highlight description html element", function (){
            var test_values;
            var description_element;
                
            // Description key exists
            test_values = { briefHeadline: "Test description" };
            description_element = this.popup.createHighlightVideoDescriptionElement(test_values);
            expect($(description_element).hasClass("highlight-description")).toEqual(true);
            expect($(description_element).text()).toBe(test_values.briefHeadline);

            // Missing the headline key
            test_values = {};
            description_element = this.popup.createHighlightVideoDescriptionElement(test_values);
            expect($(description_element).hasClass("highlight-description")).toEqual(true);
            expect($(description_element).text()).toBe("");
        });

        // createWeekSelectionButton
        it("Should create an html selection dropdown", function (){
            var test_popup;
            var dropdown_element;

            var valid_weeks_to_test = [1, 2, 8, 10, 100];
            var invalid_weeks_to_test = [-10, -1, 0, "something", null, undefined];

            valid_weeks_to_test.forEach(function (week){
                test_popup = new HighlightPopup(week, {});
                
                dropdown_element = test_popup.createWeekSelectionButton();
                expect($(dropdown_element).hasClass("week-select-dropdown")).toEqual(true);
                expect($(dropdown_element).find("select > option").length).toBe(week);
            });

            invalid_weeks_to_test.forEach(function (week){
                test_popup = new HighlightPopup(week, {});
                
                dropdown_element = test_popup.createWeekSelectionButton();
                expect($(dropdown_element).hasClass("week-select-dropdown")).toEqual(true);
                expect($(dropdown_element).find("select > option").length).toBe(0);
            });
        });

        // createHighlightsPopupBox
        it("Should create the highlight popup html element", function (){
            spyOn(this.popup.visuals, "createArrowImage");
            spyOn(this.popup, "createWeekSelectionButton").and.returnValue("<div class=\"test-dropdown\"></div>");

            var popup_element = this.popup.createHighlightsPopupBox();

            // Make sure both functions were called
            expect(this.popup.visuals.createArrowImage).toHaveBeenCalled();
            expect(this.popup.createWeekSelectionButton).toHaveBeenCalled();
            
            // Title must contain the player name and id
            expect($(popup_element).find("#highlights-title > #highlight-player-name").length).toBe(1);
            expect($(popup_element).find("#highlights-title > #highlight-player-id").length).toBe(1);

            // Header must exist and have a title and dropdown
            expect($(popup_element).find(".highlights-header").length).toBe(1);
            expect($(popup_element).find(".highlights-header > #highlights-title").length).toBe(1);
            expect($(popup_element).find(".highlights-header > .test-dropdown").length).toBe(1);
            
            // Main visible popup must exist and have a header 
            expect($(popup_element).find(".highlights-popup").length).toBe(1);
            expect($(popup_element).find(".highlights-popup > .highlights-header").length).toBe(1);

            // Main container must exist, be hidden and contain the visible popup window and arrow image             
            expect($(popup_element).hasClass("highlights-popup-container")).toEqual(true);
            expect($(popup_element).css("display")).toBe("none");
            expect($(popup_element).find(".highlights-popup").length).toBe(1);
            expect($(popup_element).find(".test-dropdown").length).toBe(1);
        });

        // createHighlightsBitrateOptions
        it("Should create the bitrate options html element", function (){
            var test_values;
            var bitrate_options_element;

            // Under 2 available bitrate data
            test_values = [{ bitrate: 1, videoPath: "Some path" }];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect(bitrate_options_element).toEqual(false);  

            test_values = [];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect(bitrate_options_element).toEqual(false);  

            // None of the bitrate options (700000, 2000000 and 5000000) are available 
            test_values = [
                { bitrate: 1000, videoPath: "some path" },
                { bitrate: 0, videoPath: "some path" }
            ];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect(bitrate_options_element).toEqual(false);  

            // More than one option but two of the same bitrates
            test_values = [
                { bitrate: 5000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some other path" }
            ];
            expect(bitrate_options_element).toEqual(false);  

            // More than one option but three of the same bitrates
            test_values = [
                { bitrate: 5000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some other path" },
                { bitrate: 5000000, videoPath: "some other other path" }
            ];
            expect(bitrate_options_element).toEqual(false);  

            // More than one option and two valid bitrate options are available
            test_values = [
                { bitrate: 700000, videoPath: "some path" },
                { bitrate: 2000000, videoPath: "some path" }
            ];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect($(bitrate_options_element).hasClass("highlight-bitrate-options")).toEqual(true);
            expect($(bitrate_options_element).find(".bitrate-option").length).toEqual(2);

            // More than one option and three valid bitrate options are available
            test_values = [
                { bitrate: 700000, videoPath: "some path" },
                { bitrate: 2000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some path" }
            ];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect($(bitrate_options_element).hasClass("highlight-bitrate-options")).toEqual(true);
            expect($(bitrate_options_element).find(".bitrate-option").length).toEqual(3);

            // More than one option and four valid bitrate options are available (two with the same bitrate)
            test_values = [
                { bitrate: 700000, videoPath: "some path" },
                { bitrate: 2000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some other path" }
            ];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect($(bitrate_options_element).hasClass("highlight-bitrate-options")).toEqual(true);
            expect($(bitrate_options_element).find(".bitrate-option").length).toEqual(3);

            // More than one option and four valid bitrate options are available (three with the same bitrate)
            test_values = [
                { bitrate: 700000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some path" },
                { bitrate: 5000000, videoPath: "some other path" },
                { bitrate: 5000000, videoPath: "some other other path" }
            ];
            bitrate_options_element = this.popup.createHighlightsBitrateOptions(test_values);
            expect($(bitrate_options_element).hasClass("highlight-bitrate-options")).toEqual(true);
            expect($(bitrate_options_element).find(".bitrate-option").length).toEqual(2);
        });

        // createHighlightElement
        it("Should create the highlight box html element", function (){
            var self = this;

            var test_values;
            var highlight_box_element;

            spyOn(this.popup, "createClickableHighlightPictureElement").and.returnValue("<div>Test picture</div>"); 
            spyOn(this.popup, "createHighlightVideoDescriptionElement").and.returnValue("<div>Test description</div>"); 
            spyOn(this.popup, "createHighlightsBitrateOptions"); 

            // Test for when the videoFileUrl key in the functions parameter object is non existant
            highlight_box_element = self.popup.createHighlightElement({});
            expect(self.popup.createClickableHighlightPictureElement).not.toHaveBeenCalled();
            expect(self.popup.createHighlightVideoDescriptionElement).not.toHaveBeenCalled();
            expect(self.popup.createHighlightsBitrateOptions).not.toHaveBeenCalled();
            
            // Test for when the videoFileUrl key in the functions parameter object is empty
            highlight_box_element = self.popup.createHighlightElement({ videoFileUrl: null });
            expect(self.popup.createClickableHighlightPictureElement).not.toHaveBeenCalled();
            expect(self.popup.createHighlightVideoDescriptionElement).not.toHaveBeenCalled();
            expect(self.popup.createHighlightsBitrateOptions).not.toHaveBeenCalled();

            // Tests for when the videoFileUrl key in the functions parameter object is exists
            test_values = [
                { return_value: false, expected_div_count: 2 },
                { return_value: "<div>Test bitrates</div>", expected_div_count: 3 }
            ];
            test_values.forEach(function (test){
                self.popup.createHighlightsBitrateOptions.and.returnValue(test.return_value);
                highlight_box_element = self.popup.createHighlightElement({ videoFileUrl: "test url" });

                expect(self.popup.createClickableHighlightPictureElement).toHaveBeenCalled();
                expect(self.popup.createHighlightVideoDescriptionElement).toHaveBeenCalled();
                expect(self.popup.createHighlightsBitrateOptions).toHaveBeenCalled();

                expect($(highlight_box_element).hasClass("video-container")).toEqual(true);
                expect($(highlight_box_element).find(" > .highlight-elements").length).toBe(1);
                expect($(highlight_box_element).find(" > .highlight-elements > div").length).toBe(test.expected_div_count);
            });
        });
        
        // createHighlightsTableElement
        it("Should create the highlights table html element", function(){
            var self = this;
            var highlights_table_element;
            
            // Scroll are initially hidden
            $("#test-container").append("<div class=\"left-scroll-arrow scroll-hidden\"></div><div class=\"right-scroll-arrow scroll-hidden\"></div>");

            var test_values = [
                { week: 1, player_id: 1000, data: {}, spy_called: false, scroll_should_be_visible: false }, 
                { week: 1, player_id: 1000, data: { 2000: { 1: ["Data"] } }, spy_called: false, scroll_should_be_visible: false }, 
                { week: 1, player_id: 1000, data: { 1000: { 2: ["Data"] } }, spy_called: false, scroll_should_be_visible: false }, 
                { week: 1, player_id: 1000, data: { 1000: { 1: ["Data"] } }, spy_called: true, scroll_should_be_visible: false },
                { week: 1, player_id: 1000, data: { 1000: { 1: ["Data 1", "Data 2", "Data 3", "Data 4"] } }, spy_called: true, scroll_should_be_visible: true }
            ];

            test_values.forEach(function (test){
                self.popup = new HighlightPopup(test.week, test.data);
                spyOn(self.popup, "createHighlightElement").and.returnValue("<div id=\"test-element\">Test element</div>");

                highlights_table_element = self.popup.createHighlightsTableElement(test.player_id, test.week);

                expect($(highlights_table_element).hasClass("highlight-table-container")).toEqual(true);
                
                // The spy function is called if the data's key is the same as the player_id and if the key of the player_id
                // is the same as the week
                if (test.spy_called){
                    expect(self.popup.createHighlightElement).toHaveBeenCalled();
                    expect($(highlights_table_element).find(" > #no-highlights-text").length).toBe(0);
                } else {
                    expect(self.popup.createHighlightElement).not.toHaveBeenCalled();
                    expect($(highlights_table_element).find(" > #no-highlights-text").length).toBe(1);
                }

                // Make sure the scrolls are visible if there's more than three data values
                if (test.scroll_should_be_visible){
                    expect($(".left-scroll-arrow").hasClass("scroll-hidden")).toEqual(false);
                    expect($(".right-scroll-arrow").hasClass("scroll-hidden")).toEqual(false);
                } else {
                    expect($(".left-scroll-arrow").hasClass("scroll-hidden")).toEqual(true);
                    expect($(".right-scroll-arrow").hasClass("scroll-hidden")).toEqual(true);
                }
            });
        });
    });

    describe("DOM events tests", function (){
        beforeEach(function (){
            this.popup = new HighlightPopup(1, {});
            $("body").append("<div id=\"test-container\"></div>", "<div class=\"highlights-popup\"></div>");
        });

        afterEach(function (){
            $("#test-container").remove();
            $(".highlights-popup").remove();
        });

        // setHighlightLinkEvents
        it("Should set the highlight image link events", function (){
            spyOn(this.popup, "createHighlightsTableElement");
            spyOn(this.popup, "displayHighlightsTable");
            spyOn(this.popup, "updatePopupHeader");
            spyOn(this.popup, "setPopupPositionOnPage");

            $("#test-container").append(
                "<div class=\"Ov-h\"><div class=\"highlight-link-image-container\"></div></div>",
                "<div class=\"highlights-popup-container\"></div>"
            );     

            // Setting the test player
            $(".Ov-h").append("<div class=\"ysf-player-name\"><a class=\"name\" href=\"/test/1234\">Test name</a></div>");
            
            var mouseup_event = {
                type: "mouseup",
                stopPropagation: function (){}
            }
            var mouseup_propagation_spy = spyOn(mouseup_event, "stopPropagation");

            var click_event = {
                type: "click",
                stopPropagation: function (){}
            }
            var click_propagation_spy = spyOn(click_event, "stopPropagation");

            this.popup.setHighlightLinkEvents();
            
            // Mouseup test
            $(".highlight-link-image-container").trigger(mouseup_event);
            expect(mouseup_propagation_spy).toHaveBeenCalled();

            // Click test
            $(".highlight-link-image-container").trigger(click_event);
            expect(click_propagation_spy).toHaveBeenCalled();

            // Popup of player link already displayed test (should be toggled to hidden since it's currently displayed)
            $(".highlights-popup-container").append("<div id=\"highlight-player-id\">1234</div>");
            $(".highlight-link-image-container").trigger(click_event);
            expect($(".highlights-popup-container").css("display")).toBe("none");
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(false);

            // Popup of player link not displayed test (should be toggled to block since it's currently hidden)
            $(".highlight-link-image-container").trigger(click_event);
            expect($(".highlights-popup-container").css("display")).toBe("block");
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(true);

            // Highlight link of a different player than the one currently displayed test
            $("#highlight-player-id").text("9999"); 
            $(".highlight-link-image-container").trigger(click_event);
            expect(this.popup.createHighlightsTableElement).toHaveBeenCalled();
            expect(this.popup.displayHighlightsTable).toHaveBeenCalled();
            expect(this.popup.updatePopupHeader).toHaveBeenCalled();
            expect(this.popup.setPopupPositionOnPage).toHaveBeenCalled();
        });

        // setDocumentEvents
        it("Should set the document events", function (){
            $("#test-container").append(
                "<div class=\"highlight-link-image-container active-popup\"></div>",
                "<div class=\"highlights-popup\"><div class=\"highlights-popup-container\"></div></div>",
                "<div class=\"invalid-test-element\"></div>"
            );

            this.popup.setDocumentEvents();
            
            // Click inside the valid containers
            $(".highlight-link-image-container").click();
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(true);
            expect($(".highlights-popup-container").css("display")).not.toBe("none");

            $(".highlights-popup").click();
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(true);
            expect($(".highlights-popup-container").css("display")).not.toBe("none");

            $(".highlights-popup-container").click();
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(true);
            expect($(".highlights-popup-container").css("display")).not.toBe("none");

            // Click outside the valid containers
            $(".invalid-test-element").click();
            expect($(".highlight-link-image-container").hasClass("active-popup")).toEqual(false);
            expect($(".highlights-popup-container").css("display")).toBe("none");
        });
        
        // setWeekSelectionDropdownEvents
        it("Should set the dropdown event listener", function (){
            spyOn(this.popup, "createHighlightsTableElement").and.returnValue("<div class=\"highlight-table-container\"><div>Test</div></div>");
            
            var select_html_element = "<select><option value=\"1\"></option><option value=\"2\"></option></select>"; 
            $("#test-container").append("<div class=\"week-select-dropdown\">" + select_html_element + "</div>");
            
            // Selection dropdown was not changed
            this.popup.setWeekSelectionDropdownEvents();
            expect(this.popup.createHighlightsTableElement).not.toHaveBeenCalled();
            
            // On selection dropdown change, the table is created and appended
            $(".week-select-dropdown > select").val("1").change();
            expect(this.popup.createHighlightsTableElement).toHaveBeenCalled();
            expect($(".highlights-popup .highlight-table-container").length).toBe(1);
            expect($(".week-select-dropdown > select").val()).toBe("1");

            // On selection dropdown change again, make sure there is only one table container, i.e. the new one replaced the old one 
            $(".week-select-dropdown > select").val("2").change();
            expect(this.popup.createHighlightsTableElement).toHaveBeenCalled();
            expect($(".highlights-popup .highlight-table-container").length).toBe(1);
            expect($(".week-select-dropdown > select").val()).toBe("2");
        });
    });

    describe("Data manipulation functions", function (){
        beforeEach(function (){
            this.popup = new HighlightPopup(1, {});
        });

        it("Should return the dimensions to set for the single video popup", function (){
            var self = this;

            var test_values = [
                { bitrate: 0, expected_output: { height: 0, width: 0 } },
                { bitrate: null, expected_output: { height: 0, width: 0 } },
                { bitrate: undefined, expected_output: { height: 0, width: 0 } },
                { bitrate: "something", expected_output: { height: 0, width: 0 } },
                { bitrate: 700, expected_output: { height: 0, width: 0 } },
                { bitrate: 700000, expected_output: { height: 380, width: 660 } },
                { bitrate: 2000000, expected_output: { height: 452, width: 788 } },
                { bitrate: 5000000, expected_output: { height: 708, width: 1244 } }
            ];

            test_values.forEach(function (test){
                expect(self.popup.getDimentionsToSetForSingleVideoPopup(test.bitrate)).toEqual(test.expected_output);
            });
        });

        it("Should return the positions to set the single video popup", function (){
            var self = this;
            
            // Set the window's size for testing
            window.outerWidth = 1410; 
            window.innerWidth = 1400; 
            window.outerHeight = 950; 
            window.innerHeight = 900; 
            //spyOn(window, "outerWidth").and.returnValue(1410); 
            //spyOn(window, "innerWidth").and.returnValue(1400); 
            //spyOn(window, "outerHeight").and.returnValue(550); 
            //spyOn(window, "innerHeight").and.returnValue(500); 

            var test_values = [
                { bitrate: 0, expected_output: { top: 0, left: 0 } },
                { bitrate: null, expected_output: { top: 0, left: 0 } },
                { bitrate: undefined, expected_output: { top: 0, left: 0 } },
                { bitrate: "something", expected_output: { top: 0, left: 0 } },
                { bitrate: 700, expected_output: { top: 0, left: 0 } },
                { bitrate: 700000, expected_output: { top: 300, left: 380 } },
                { bitrate: 2000000, expected_output: { top: 264, left: 316 } },
                { bitrate: 5000000, expected_output: { top: 136, left: 88 } }
            ];

            test_values.forEach(function (test){
                expect(self.popup.getPositionsToSetForSingleVideoPopup(test.bitrate)).toEqual(test.expected_output);
            });
        });
    });    
});
