# Development
## Dependencies
* [Webpack](https://github.com/webpack/webpack)
* [JQuery](https://jquery.com/download/)
* [jasmine](https://jasmine.github.io/)  
* [karma](https://karma-runner.github.io/2.0/index.html)  

## Tools
* [npm](https://www.npmjs.com/get-npm)  

## Setting up the environment
### Cloning the repository
Using https (replace [username] with your bitbucket username)  
```
git clone https://[username]@bitbucket.org/jFilteau/yahoo-fantasy-highlights.git
```  
Using ssh  
```
git clone git@bitbucket.org:jFilteau/yahoo-fantasy-highlights.git
```  

### Installing dependencies
Navigate into the project folder  
```
cd yahoo-fantasy-highlights
```  
And then install the dependencies with npm  
```
npm install
```  

## Running the application
Before running the application, we must build the necessary files.  
To do so, run:  
```
npm run build
```  
The required files will then be created in the `dist/` folder.  
  
If you don't want to run the build after every file change, you can watch the files for any modifications.  
To do so, run:  
```
npm run watch
```

## Running tests
Tests are found in the test folder and have the file naming pattern: [module name to test].test.js  
To run the tests, do:  
```
npm test
```
