import * as $ from "jquery";
import { team_abbr_to_team_id } from "./mappings/teamAbbreviations.js";


function playersInvolvedInTheHighlightPlay(play, player_names_to_id_and_team_abbr){
    var players_involved_in_the_play;
    var display_name_regex;
    var player_name_regex;
    var yahoo_team_abbr;
    var player_ids_from_fantasy_team_involved = [];
    var player_not_part_of_teams_involved_in_play;

    var teams_involved_in_play = play.teams;
    
    // We have to know the team involved to know if it's a player's highlight or lowlight,
    // the players involved and the caption (description) of the play
    if (play.teams == null || play.players == null || play.briefHeadline == null){
        return false;
    }

    for (var player_name in player_names_to_id_and_team_abbr){
        // The team involved must be that of the player since it represents a positive highlight of theirs.
        // e.g. If Tom Brady throws an INT against the Panthers, the team ID associated to the play would
        // be the ID of the Panthers, therefore it does not represent a highlight for Brady
        yahoo_team_abbr = player_names_to_id_and_team_abbr[player_name]["team_abbr"].trim().toUpperCase();
        
        // In case of multiple teams associated to a play, we check that all teams are not associated with
        // the player. If one of them is, the every loop  will return false and we can continue with the filters
        player_not_part_of_teams_involved_in_play = teams_involved_in_play.every(function (team){
            return team.teamId !== team_abbr_to_team_id[yahoo_team_abbr];
        });

        if (player_not_part_of_teams_involved_in_play){
            continue;
        }

        // We must check the caption since the players key of the play does not always have all the main
        // players that were part of the play (e.g. if Tom Brady throws a TD to Edelman, the players key
        // might only contain one of those two players).
        player_name_regex = new RegExp(player_name, "g");
        if (player_name_regex.test(play.caption)){
            player_ids_from_fantasy_team_involved.push(player_names_to_id_and_team_abbr[player_name]["id"]);
            continue;
        }

        // If the player is not mentionned in the caption (i.e. fails the above test), they can still be
        // part of the play if they're in the players key values of the play object
        players_involved_in_the_play = play.players;
        players_involved_in_the_play.forEach(function (involved_player){
            if (!Object.keys(involved_player).includes("displayName")){
                return;
            }

            // The display name from the data returned from the API sometimes isn't the actual full name (e.g.
            // missing a JR), so we'll use a regex to see if the yahoo name contains the API display name
            display_name_regex = new RegExp(involved_player.displayName, "g");

            if (display_name_regex.test(player_name)){
                player_ids_from_fantasy_team_involved.push(player_names_to_id_and_team_abbr[player_name]["id"]);
            }
        });
    }

    if (player_ids_from_fantasy_team_involved.length == 0){
        return false;
    }

    return player_ids_from_fantasy_team_involved;
}

function searchHighlightVideosForEachPlayer(play, players){
    var player_id_to_week_highlights = {};
    var fantasy_player_ids_involved_in_highlight = playersInvolvedInTheHighlightPlay(play, players);
        
    // None of the fantasy team's players were involved in the play 
    if (!fantasy_player_ids_involved_in_highlight){
        return false;
    } 

    fantasy_player_ids_involved_in_highlight.forEach(function (id){
        player_id_to_week_highlights[id] = play;
    });

    return player_id_to_week_highlights;
}

function getHighlightsData(start_week, end_week, players, callback, stored = null){
    var base_url = "http://www.nfl.com/feeds-rs/videos/byChannel/nfl-game-highlights//byWeek/2018/REG/";
    var week_to_fetch = start_week;
    var get_requests = [];

    function getNextWeekData(){
        if (week_to_fetch > end_week){
            $.when.apply($, get_requests).done(function (...request_data){
                var data_to_send;
                var all_plays = [];
                
                // If there was only one ajax request, the arguments won't represent multiple responses but only one.
                // Multiple responses -> [[{...}, "success", {...}], [{...}, "success", {...}], ...] -> Array of arrays
                // One single response -> [{...}, "success", {...}] -> One array
                if (get_requests.length === 1){
                    all_plays = all_plays.concat(request_data[0].videos);
                } else {
                    request_data.forEach(function (data){
                        // Only append if there are highlights
                        if (data[0].videos.length !== 0){
                            all_plays = all_plays.concat(data[0].videos);
                        }
                    });
                }

                data_to_send = filterHighlightPlaysByType(all_plays, players);

                if (stored){
                    for (var key in stored){
                        if (data_to_send[key] == null){
                            data_to_send[key] = stored[key];
                        } else {
                            // Update the data if there are new highlights for a given key (i.e. player or game recap)
                            data_to_send[key] = Object.assign(stored[key], data_to_send[key]); 
                        }
                    }
                }

                // Send the response back to the content script
                callback({ data: data_to_send });
            });

            return;
        }
       
        // The initial request is needed to be able to get the total number of videos for the week.
        // From this total, we'll be able to set the offset in the while loop or skip the week altogether
        // if there's no highlights
        $.ajax({ url: base_url + week_to_fetch + ".json?limit=100&offset=0" }).then(function (data){
            var offset = 0;
            var weekly_highlight_video_count = data.total;
            
            // Make more requests only if the week has highlights
            if (weekly_highlight_video_count !== 0){
                // Since every week has a dfferent number of highlights, the limit is set to the weekly total.
                // The maximum number of videos returned by the api is 100, therefore we'll increment by 100
                // until it passes the limit.
                while (offset <= weekly_highlight_video_count){
                    get_requests.push($.ajax({ url: base_url + week_to_fetch + ".json?limit=100&offset=" + offset }));
                    offset += 100;
                }
            }
            
            week_to_fetch += 1;
            getNextWeekData();
        });
    } 
    
    getNextWeekData();
}

function allPlayerHighlightsOnTeamAreStored(players, stored){
    if (stored === null){
        return false;
    }
 
    for (var player in players){
        // If even one player is not stored, returning false means we'll later have to do an ajax call for every week of the season
        // to get all of their highlights for the year
        if (!Object.keys(stored).includes(players[player]["id"].toString())){
            return false;
        } 
    }

    return true;
}

function getHighlightClipType(play){
    if (!Object.keys(play).includes("clipType")){
        return false;
    }

    return play["clipType"];
}

function getTeamIdsInvolvedInGameRecap(teams){
    var team_ids = [];

    if (!teams){
        return false;
    }

    teams.forEach(function (team){
        if (!Object.keys(team).includes("teamId")){
            return;
        }

        team_ids.push(team["teamId"]);
    });

    return team_ids
}

function searchGameRecapHighlight(play){
    var team_ids_involved_in_highlight;
    var game_recap_values = {};
    var keys_needed = ["videoFileUrl", "teams", "week"];

    var highlight_clip_type = getHighlightClipType(play);
    if (!highlight_clip_type || highlight_clip_type.trim().toLowerCase() != "game-highlight"){
        return false;
    }

    for (var i = 0; i < keys_needed.length; i++){
        // The recap is invalid if even one of the required keys is missing or has an empty value associated to it
        if (!Object.keys(play).includes(keys_needed[i]) || play[keys_needed[i]] == null){
            return false;
        }
    }

    team_ids_involved_in_highlight = getTeamIdsInvolvedInGameRecap(play["teams"]);
    if (!team_ids_involved_in_highlight || team_ids_involved_in_highlight.length == 0){
        return false;
    }

    game_recap_values["recap"] = {
        "teams": getTeamIdsInvolvedInGameRecap(play["teams"]),
        "url": play["videoFileUrl"]
    };
    game_recap_values["week"] = play["week"];

    return game_recap_values;
}

function filterHighlightPlaysByType(plays, players){
    var highlights_by_type = {};
    var single_play_highlight;
    var game_recap_highlight;

    const update_highlights_by_type_dict = (key, week, value) => {
        // Since object properties must be a string and not an integer
        let converted_key = key.toString();
        let converted_week = week.toString();

        // Highlight of key does not exist
        if (!Object.keys(highlights_by_type).includes(converted_key)){ 
            highlights_by_type[converted_key] = { [converted_week]: [value] };
            return;
        }

        // Highlight of key exists but not for the given week
        if (!Object.keys(highlights_by_type[converted_key]).includes(converted_week)){
            highlights_by_type[converted_key][converted_week] = [value];
            return; 
        }
        
        // Highlight of key exists for the given week, so update the array of highlights
        highlights_by_type[converted_key][converted_week].push(value);
    };

    plays.forEach(function (play){
        // Single play highlights have a playId value
        if (play.playId != null){
            single_play_highlight = searchHighlightVideosForEachPlayer(play, players);

            // None of the players involved in the play are on the fantasy team, so move on to the next play
            if (!single_play_highlight){
                return;
            }

            for (var player_id in single_play_highlight){
                update_highlights_by_type_dict(player_id, play.week, single_play_highlight[player_id]);
            }

            return;
        }

        game_recap_highlight = searchGameRecapHighlight(play);

        // There are other types of highlights, so make sure it's a game recap
        if (game_recap_highlight){
            update_highlights_by_type_dict("game_recaps", game_recap_highlight.week, game_recap_highlight.recap);
        }
    });

    return highlights_by_type;
}

function getOnlyNeededHighlightsFromStorage(players, stored){
    var game_recaps = getGameRecapHighlights(stored);
    var player_highlights = getPlayersStoredHighlights(players, stored);

    // Don't have to worry about overriding data in the merge since the game_recaps key is "game_recaps" whereas the player highlights
    // is an integer value representing the player's id
    return Object.assign(game_recaps, player_highlights);
}

function getGameRecapHighlights(stored){
    var game_recaps = {};

    if (!Object.keys(stored).includes("game_recaps")){
        return game_recaps;
    }
    
    game_recaps["game_recaps"] = stored.game_recaps;

    return game_recaps;
}

function getPlayersStoredHighlights(players, stored){
    var id;
    var highlights = {};

    for (var player in players){
        if (!Object.keys(players[player]).includes("id")){
            continue;
        }

        id = parseInt(players[player]["id"]);
        highlights[id] = stored[id];    
    }

    return highlights;
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse){
    var stored_values_needed;
    var players = request.players;

    // If one or more players on the team don't have highlights stored or if the game recaps are not stored, we don't have a choice
    // but to get the highlights data for the whole season, i.e. starting from week 1
    if (!allPlayerHighlightsOnTeamAreStored(players, request.stored_highlights) || !Object.keys(request.stored_highlights).includes("game_recaps")){
        getHighlightsData(1, request.week, players, sendResponse);
    } else {
        stored_values_needed = getOnlyNeededHighlightsFromStorage(players, request.stored_highlights);

        // Since we retrieved the data from storage, the only data to fetch is this week's
        getHighlightsData(request.week, request.week, players, sendResponse, stored_values_needed);
    }
    
    // Keep the channel open (important since we're doing asynchronous calls)
    return true;
});
