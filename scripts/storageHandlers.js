function filterSingleHighlight(highlight){
    var valid_bitrates = [700000, 2000000, 5000000];
    var new_highlight_data = {};
    
    // This can be modified at any time if there are other values needed for new features
    var valid_data_keys_to_keep = ["briefHeadline", "mediumImageUrl", "players", "teams", "videoBitRates", "videoFileUrl"];

    for (var key in highlight){
        let bitrate_options = [];

        if (!valid_data_keys_to_keep.includes(key)){
            continue;
        }

        if (key == "videoBitRates"){
            for(var option in highlight[key]){
                // There are more bitrate options than the three we want to offer, so we'll discard those
                // that are not needed
                if (!valid_bitrates.includes(highlight[key][option].bitrate)){
                    continue;
                }

                bitrate_options.push(highlight[key][option]);
            }
        
            new_highlight_data[key] = bitrate_options;
            continue;
        }

        new_highlight_data[key] = highlight[key];
    }

    return new_highlight_data;
}

function createUpdatedArrayOfHighlights(highlights){
    var new_single_highlight_data;
    var new_highlights_array = [];

    if (!Array.isArray(highlights)){
        return false;
    }

    highlights.forEach(function (highlight){
        // A highlight is an object value cotaining the data we need
        if (highlight == null || highlight.constructor !== Object){
            return;
        }

        // Filter out any keys that are not needed to save space for when we save it in the local storage
        new_single_highlight_data = filterSingleHighlight(highlight);

        if (!new_single_highlight_data || Object.keys(new_single_highlight_data).length === 0){
            return;
        }

        new_highlights_array.push(new_single_highlight_data); 
    });

    return new_highlights_array;
}

function createFilteredDataToStore(data){
    var new_data_to_store = {};
    var highlights_by_week;
    var updated_array_of_highlights;

    if (data == null || data.constructor !== Object || Object.keys(data).length === 0){
        return new_data_to_store;
    }

    for (var key in data){
        // Game recaps are already filtered
        if (key == "game_recaps"){
            new_data_to_store[key] = data[key];
            continue;
        }

        new_data_to_store[key] = {}; 
        highlights_by_week = data[key];

        // Player has no highlights all season (cmon step your game up!)
        if (Object.keys(highlights_by_week).length === 0){
            continue;
        }

        for (var week in highlights_by_week){
            updated_array_of_highlights = createUpdatedArrayOfHighlights(highlights_by_week[week]);

            // No point in appending new highlights for a given week if there are none, i.e. the new array is empty
            if (!updated_array_of_highlights || updated_array_of_highlights.length === 0){
                continue;
            }

            new_data_to_store[key][week] = updated_array_of_highlights;
        } 
    }

    return new_data_to_store;
}

function updateHighlightsStorageData(highlights){
    var stored_highlights = localStorage.getItem("highlights") === null ? null : JSON.parse(localStorage.getItem("highlights"));

    // If the key does not exist, we simply create it and set its value to the highlights data
    if (stored_highlights === null){
        localStorage.setItem("highlights", JSON.stringify(highlights));
        return;
    }

    for (var key in highlights){
         //Since the highlights parameter is the most updated data, we'll replace what's stored with it
        stored_highlights[key] = highlights[key];
    }

    localStorage.setItem("highlights", JSON.stringify(stored_highlights));
}

export { createFilteredDataToStore, updateHighlightsStorageData };
