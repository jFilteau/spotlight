import { createFilteredDataToStore } from "../scripts/storageHandlers.js";


describe("Storage handler tests", function (){
    it("Should create a dict with only the needed values for storage", function (){
        var invalid_data_test_values = [
            { data: null, expected_output: {} },
            { data: 1234, expected_output: {} },
            { data: "invalid", expected_output: {} },
            { data: {}, expected_output: {} }
        ];
        invalid_data_test_values.forEach(function (test){
            expect(createFilteredDataToStore(test.data)).toEqual(test.expected_output);
        });

        var missing_week_test_values = [{ data: { 1234: {} }, expected_output: { 1234: {} } }];
        missing_week_test_values.forEach(function (test){
            expect(createFilteredDataToStore(test.data)).toEqual(test.expected_output);
        });

        var invalid_week_highlights_data = [
            { data: { 1234: { 1: null } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: "invalid" } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: {} } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: [] } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: [null] } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: ["invalid"] } }, expected_output: { 1234: {} } }
        ];
        invalid_week_highlights_data.forEach(function (test){
            expect(createFilteredDataToStore(test.data)).toEqual(test.expected_output);
        });
        
        var valid_data = [
            { data: { 1234: { 1: [{ "invalid": "data" }] } }, expected_output: { 1234: {} } },
            { data: { 1234: { 1: [{ "briefHeadline": "data" }] } }, expected_output: { 1234: { 1: [{ "briefHeadline": "data" }] } } },
            { data: { 1234: { 1: [{ "briefHeadline": "data" }, { "invalid": "data" }] } }, expected_output: { 1234: { 1: [{ "briefHeadline": "data" }] } } },
            { data: { 1234: { 1: [{ "videoBitRates": [{ "bitrate": 99999 }] }] } }, expected_output: { 1234: { 1: [{ "videoBitRates": [] }] } } },
            { data: { 1234: { 1: [{ "videoBitRates": [{ "bitrate": 700000 }] }] } }, expected_output: { 1234: { 1: [{ "videoBitRates": [{ "bitrate": 700000 }] }] } } }
        ];
        valid_data.forEach(function (test){
            expect(createFilteredDataToStore(test.data)).toEqual(test.expected_output);
        });
    });
});

