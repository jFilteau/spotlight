import * as $ from "jquery";
import { getCurrentWeekNumber, getPlayersAndTeamOnFantasyTeam } from "../content.js";


describe("Content script functions test", function (){
    beforeEach(function (){
        $("body").append("<div id=\"test-container\"></div>");
    });

    afterEach(function (){
        $("#test-container").remove();
    });

    it("Should return the current week", function (){
        var test_values;

        // Week tag doesn't exist
        expect(function (){ getCurrentWeekNumber() }).toThrowError("Current week tag not found."); 

        // Week tag exists now
        $("#test-container").append("<div class=\"flyout-title\"></div>");

        test_values = [
            { text: "", error: "Text inside the week tag is invalid." },
            { text: "Invalid", error: "Text inside the week tag is invalid." },
            { text: "Invalid Text", error: "Week value inside the week tag is not a number." },
            { text: "Week 2", expected_return: 2 },
            { text: "Week -1", expected_return: -1 }
        ];

        test_values.forEach(function (test){
            $(".flyout-title").text(test.text);

            if("error" in test){
                expect(function (){ getCurrentWeekNumber() }).toThrowError(test.error); 
                return;
            }

            expect(getCurrentWeekNumber()).toBe(test.expected_return);
        });
    });

    it("Should return the players on the team", function (){
        var test_values;

        // Player tag doesn't exist
        expect(function (){ getPlayersAndTeamOnFantasyTeam() }).toThrowError("Player tags not found."); 

        // Player tag exists now but with no <a> tag for the player's name
        $("#test-container").append("<div class=\"ysf-player-name\"></div>");
        expect(function (){ getPlayersAndTeamOnFantasyTeam() }).toThrowError("Player tags don't contain the player name tag."); 

        // Player tag exists now with the <a> tag for the player's name but no tag for the player's team and position
        $(".ysf-player-name").append("<a class=\"name\"></a>");
        expect(function (){ getPlayersAndTeamOnFantasyTeam() }).toThrowError("Player tags don't contain the team and position tag."); 

        // Player tag exists now with the <a> tag for the player's name and the tag for the player's team and position
        $(".ysf-player-name").append("<span class=\"Fz-xxs\"></span>");

        test_values = [
            { name: "Test", team_and_pos: "", error: "Player position and team string does not contain two elements separated by a dash." },
            { name: "Test", team_and_pos: "Team", error: "Player position and team string does not contain two elements separated by a dash." },
            { name: "Test", team_and_pos: "20", error: "Player position and team string does not contain two elements separated by a dash." },
            { name: "Test", team_and_pos: "NE QB", error: "Player position and team string does not contain two elements separated by a dash." },
            { name: "Test", team_and_pos: "ATL - INVALID", href: "", expected_output: {} },
            { name: "Test", team_and_pos: "INVALID - QB", href: "", expected_output: {} },
            { name: "Test", team_and_pos: "ATL - DEF", href: "", expected_output: {} },
            { name: "Test", team_and_pos: "ATL - QB", href: "invalid", expected_output: {} },
            { name: "Test", team_and_pos: "ATL - QB", href: "/invalid", expected_output: {} },
            { name: "Test", team_and_pos: "ATL - QB", href: "/1234", expected_output: { "Test": { "team_abbr": "ATL", "id": 1234 } } }
        ];
        test_values.forEach(function (test){
            $(".ysf-player-name a.name").text(test.name);
            $(".ysf-player-name .Fz-xxs").text(test.team_and_pos);

            if ("error" in test){
                expect(function (){ getPlayersAndTeamOnFantasyTeam() }).toThrowError(test.error);
                return;
            }
            
            $(".ysf-player-name a.name").attr("href", test.href);
            expect(getPlayersAndTeamOnFantasyTeam()).toEqual(test.expected_output);
        });
    });
});
