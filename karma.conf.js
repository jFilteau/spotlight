module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine"],
    files: ["tests/*.test.js"],
    preprocessors: {
        "tests/*.test.js": ["webpack"]
    },
    plugins: [
        "karma-webpack",
        "karma-jasmine",
        "karma-chrome-launcher",
        "karma-mocha-reporter"
    ],
    webpack: {
        mode: "development"
    },
    reporters: ["mocha"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: false,
    concurrency: Infinity
  });
}
