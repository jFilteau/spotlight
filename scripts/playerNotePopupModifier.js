import * as $ from "jquery";
import { team_abbr_to_team_id } from "../mappings/teamAbbreviations.js";


export class PlayerNotePopupModifier {
    constructor (data){
        this.game_recaps = data;
    }

    getPlayerNoteElements(){
        var player_notes = $(".playernote");

        if (player_notes.length === 0){
            return false;
        }

        return player_notes;
    }

    getStatsTableElement(popup){
        if ($(popup).find("table.teamtable").length == 0){
            return false;
        }

        return $(popup).find("table.teamtable");
    }

    getTableRowAssociatedWeek(row){
        var week = $(row).find(".week");

        if (week.length == 0 || isNaN(Number($(week).text()))){
            return false;
        }

        return Number($(week).text());
    }

    getTableRowAssociatedMatchupOpponent(row){
        var opponent = $(row).find(".opp");

        if (opponent.length == 0 || $(opponent).text().trim().length == 0){
            return false;
        }

        // For games that were played away
        opponent = opponent.text().trim().replace("@", "").toUpperCase();

        return opponent;
    }

    getGameRecapUrl(week, player_team_id, opponent_id){
        var game_recap_url;
        var recap_teams_involved;
        var week_recaps;

        // Game recaps for the week don't exist
        if (!Object.keys(this.game_recaps).includes(week.toString())){
            return false;
        }

        week_recaps = this.game_recaps[week];
        for (var i in week_recaps){
            recap_teams_involved = week_recaps[i].teams;

            if (recap_teams_involved.includes(player_team_id) && recap_teams_involved.includes(opponent_id)){
                game_recap_url = this.game_recaps[week][i].url === null ? false : this.game_recaps[week][i].url; 
                return game_recap_url;
            }
        }

        return false;
    }

    wrapAssociatedOpponentTextAsLink(row, team_id){
        var opponent_uppercased;
        var opponent_abbreviation;
        var game_recap_link;
        var game_recap_url;

        var week = $(row).find(".week");
        var opponent = $(row).find(".opp");

        if (opponent.length == 0 || week.length == 0){
            return;
        }

        week = parseInt($(week).text().trim());
        opponent_uppercased = $(opponent).text().trim().toUpperCase();

        // Ignore empty opponents or bye week
        if (opponent_uppercased.length == 0 || isNaN(week) || opponent_uppercased == "BYE"){
            return;
        }

        opponent_abbreviation = this.getTableRowAssociatedMatchupOpponent(row);
        
        // Ignore invalid opponent abbreviations
        if (!opponent_abbreviation){
            return;
        }

        game_recap_url = this.getGameRecapUrl(week, team_id, team_abbr_to_team_id[opponent_abbreviation]);

        // No point in making the opponant clickable if there's url to open on click
        if (!game_recap_url){
            return;
        }

        game_recap_link = $("<a style=\"cursor: pointer;\"></a>");
        this.setGameRecapLinkEvent(game_recap_link, game_recap_url);

        // Make the opponent element clickable
        $(opponent).wrapInner(game_recap_link);
    }

    weekAssociatedGameStatusIsFinalized(row){
        var game_status_element = $(row).find(".status");
        var status_finalized_regex = new RegExp("final", "i");
        
        if (game_status_element.length == 0){
            return false;
        }

        return status_finalized_regex.test($(game_status_element).text());
    }

    modifyTableElements(table, team_id){
        var self = this;
        var rows = $(table).find("tbody tr");

        if (rows.length == 0){
            return;
        }

        $(rows).each(function (){
            // Game recaps are only available when games are finalized
            if (!self.weekAssociatedGameStatusIsFinalized(this)){
                return;
            }

            self.wrapAssociatedOpponentTextAsLink(this, team_id);
        });
    }

    addedNodesIncludeStatsTable(nodes){
        var stats_table_included = false;

        nodes.forEach(function (node){
            if ($(node).find(".teamtable").length == 1){
                stats_table_included = true;
            } 
        });

        return stats_table_included;
    }

    setPlayerNoteEvents(elements){
        var self = this;

        $(elements).click(function (event){
            var team_id;
            var stats_table;

            var observer = new MutationObserver(function (mutations){
                mutations.forEach(function (mutation){
                    var added_nodes = mutation.addedNodes;

                    if (added_nodes.length == 0){
                        return;
                    }

                    // Wait for the main content container to be added
                    if (!$(added_nodes[0]).hasClass("yui3-ysplayernote-bdcontent")){
                        return;
                    }
                    
                    // Wait for the stats table node to be added before continuing
                    if (!self.addedNodesIncludeStatsTable(added_nodes)){
                        return
                    }

                    stats_table = self.getStatsTableElement(added_nodes);

                    // Wait for the main table to be added
                    if (!stats_table){
                        return;
                    }

                    team_id = self.getTeamIdAssociatedToPlayerStatusIconClicked(event.target);

                    if (!team_id){
                        return;
                    }

                    self.modifyTableElements(stats_table, team_id);
                });
            }); 

            // Since the player note popup elements are a direct child of the document body, we'll add the observer to the body itself
            observer.observe(document.body, { childList: true, subtree: true });
        });
    }

    getTeamIdAssociatedToPlayerStatusIconClicked(icon){
        var team_and_position = $(icon).closest(".Grid-bind-end").find(".Fz-xxs");
        var team_abbr = $(team_and_position).text().split("-")[0].trim().toUpperCase();

        if (!team_abbr_to_team_id.hasOwnProperty(team_abbr)){
            return false;
        }

        return team_abbr_to_team_id[team_abbr];
    }

    setGameRecapLinkEvent(link, url){
        // Extra interface elements like toolbars and scrollbars
        var extra_vewport_height_dimension = window.outerHeight - window.innerHeight;
        var extra_vewport_width_dimension = window.outerWidth - window.innerWidth;

        var popup_top_position = ((window.innerHeight - 400) / 2) + extra_vewport_height_dimension;
        var popup_left_position = ((window.innerWidth - 660) / 2) + extra_vewport_width_dimension;

        var window_options = "toolbar=no, titlebar=no, location=yes, menubar=no, scrollbars=no, resizable=yes, width=660 , height=380 ,left=" + popup_left_position + ",top=" + popup_top_position;

        $(link).click(function (event){
            event.preventDefault();
            window.open(url, "_blank", window_options);
        });
    }

    init(){
        var player_notes = this.getPlayerNoteElements();

        if (!player_notes){
            return;
        }

        this.setPlayerNoteEvents(player_notes);
    }
}
